package com.ws.services;

import javax.jws.WebService;

/**
 * PUT DESCRIPTION HERE
 * 
 * @version 1.0 Initial Version
 * @author Praveen Rawat
 * @since Jun 4, 2018
 */
@WebService
public class GenericServices
{

  /**
   * @param salutation
   * @return greeting message
   */
  public String getGreetings(String salutation)
  {
    return "Welcome "+salutation+" Rawat!!!";
  }

  /**
   * @param position
   * @return returns salary
   */
  public long getSalary(String position)
  {
    return 0l;
  }

  /**
   * @param salutation
   * @return bye message
   */
  public String getByeMessage(String salutation)
  {
    return "";
  }
}
