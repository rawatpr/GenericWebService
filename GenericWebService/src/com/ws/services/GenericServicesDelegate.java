package com.ws.services;

import javax.jws.WebService;


@WebService (targetNamespace="http://services.ws.com/", serviceName="GenericServicesService", portName="GenericServicesPort")
public class GenericServicesDelegate{

    com.ws.services.GenericServices _genericServices = null;

    public String getGreetings (String salutation) {
        return _genericServices.getGreetings(salutation);
    }

    public long getSalary (String position) {
        return _genericServices.getSalary(position);
    }

    public String getByeMessage (String salutation) {
        return _genericServices.getByeMessage(salutation);
    }

    public GenericServicesDelegate() {
        _genericServices = new com.ws.services.GenericServices(); }

}